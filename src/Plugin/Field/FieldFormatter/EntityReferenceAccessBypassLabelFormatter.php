<?php

namespace Drupal\entity_reference_access_bypass\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;

/**
 * Plugin implementation of the 'entity reference access bypass label' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_access_bypass_label_formatter",
 *   description = @Translation("Display the label of the referenced entities with access bypass."),
 *   label = @Translation("Label (access bypass)"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceAccessBypassLabelFormatter extends EntityReferenceLabelFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('<mark>¡Beware!</mark> This formatter can lead to security issues.<br> Only expose this info if you are certain about what you are doing.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntitiesToView(EntityReferenceFieldItemListInterface $items, $langcode) {
    $entities = [];

    foreach ($items as $delta => $item) {
      // Ignore items where no entity could be loaded in prepareView().
      if (!empty($item->_loaded)) {
        $entity = $item->entity;

        // Set the entity in the correct language for display.
        if ($entity instanceof TranslatableInterface) {
          $entity = \Drupal::service('entity.repository')->getTranslationFromContext($entity, $langcode);
        }
        // Add the referring item, in case the formatter needs it.
        $entity->_referringItem = $items[$delta];
        $entities[$delta] = $entity;
      }
    }

    return $entities;
  }

}
